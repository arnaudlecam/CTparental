# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ctparental package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ctparental\n"
"Report-Msgid-Bugs-To: ctparental@packages.debian.org\n"
"POT-Creation-Date: 2020-03-12 23:28+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Enter login to the administration interface:"
msgstr ""

#. Type: string
#. Description
#. Type: error
#. Description
#: ../templates:1001 ../templates:2001
msgid "- Only letters or numbers. - 5 characters minimum:"
msgstr ""

#. Type: error
#. Description
#: ../templates:2001
msgid "error login:"
msgstr ""

#. Type: password
#. Description
#: ../templates:3001
msgid "Enter your password:"
msgstr ""

#. Type: password
#. Description
#. Type: password
#. Description
#: ../templates:3001 ../templates:4001
msgid "and press OK to continue."
msgstr ""

#. Type: password
#. Description
#: ../templates:4001
msgid "Confirm your password:"
msgstr ""

#. Type: error
#. Description
#: ../templates:5001
msgid "The password entered is not identical to the first."
msgstr ""

#. Type: error
#. Description
#: ../templates:6001
msgid "Password is not complex enough, it must contain at least:"
msgstr ""

#. Type: error
#. Description
#: ../templates:6001
#, no-c-format
msgid ""
"6 to 20 characters total, 1 Uppercase, lowercase 1, number 1 and one special "
"character among the following. &éè~#{}()ç_@à?.;:/!,$<>=£%"
msgstr ""

#. Type: error
#. Description
#: ../templates:7001
msgid "Internet connection required!"
msgstr ""
