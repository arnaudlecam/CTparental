# Link to WiKi

The WiKi pages can be found [here](https://gitlab.com/ctparentalgroup/CTparental/-/wikis/home "WiKi Homepage og CTparental").

These pages explain installation and usage.
